package com.example.domain.usecase.popularpurchase

import com.example.domain.entity.Product
import com.example.domain.entity.ProductDetail
import com.example.domain.repository.IUserPurchasesRepository
import com.example.domain.util.Lce
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.functions.BiFunction
import javax.inject.Inject

class GetPopularPurchasesUseCase @Inject constructor(
  private val userPurchasesRepository: IUserPurchasesRepository
) {
  fun invoke(username: String, limit: Int): Observable<Lce<PopularPurchaseViewState>> {
    return userPurchasesRepository.getPurchasesByUsername(username, limit).flatMap<Lce<PopularPurchaseViewState>> {purchases ->
      val distinctProduct = purchases.distinctBy { pc -> pc.productId }
      if (distinctProduct.isNotEmpty()) {
        val purchasedByProductObservable = Observable.fromIterable(distinctProduct).concatMap { purchase ->
          userPurchasesRepository
            .getPurchasesByProduct(purchase.productId)
            .onErrorResumeNext(Single.just(emptyList()))
            .toObservable()
        }.toList()

        val productInformationObservable = Observable.fromIterable(distinctProduct).concatMap {
          purchase -> userPurchasesRepository
            .getProductInformation(purchase.productId)
            .onErrorResumeNext(Single.just(Product(id = purchase.productId)))
            .toObservable()
        }.toList()

        val result = purchasedByProductObservable.zipWith<List<Product>, List<ProductDetail>>(
          productInformationObservable, BiFunction { purchasesByProduct, productInfo ->
            productInfo.mapIndexed { indexed, info ->
              ProductDetail(
                id = info.id, face = info.face, price = info.price, size = info.size,
                recent = purchasesByProduct[indexed].map { pch -> pch.username }
              )
            }.sortedByDescending { prd -> prd.recent.size }
          })
          .map { Lce.Content(PopularPurchaseViewState(username, it)) }
        result
      } else {
          Single.just(Lce.Content(PopularPurchaseViewState(username, emptyList())))
      }
    }.onErrorReturn { ex -> Lce.Error(PopularPurchaseViewState(username, emptyList())) }
      .toObservable()
      .startWith(Lce.Loading())
  }
}