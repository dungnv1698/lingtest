package com.example.domain.usecase.authentication

data class AuthenticationViewState(
  val username: String,
  val isAuthenticated: Boolean
)

sealed class AuthenticateViewEvent {
  data class Login(val username: String): AuthenticateViewEvent()
}

sealed class AuthenticateViewEffect {
  data class NotifyAuthenticateFail(val username: String): AuthenticateViewEffect()
  data class NavigateToPopularPurchase(val username: String): AuthenticateViewEffect()
  object NotifyErrorAuthenticate: AuthenticateViewEffect()
}
