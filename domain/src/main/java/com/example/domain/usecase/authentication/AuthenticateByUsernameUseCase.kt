package com.example.domain.usecase.authentication

import com.example.domain.repository.IUserPurchasesRepository
import com.example.domain.util.Lce
import io.reactivex.Observable
import javax.inject.Inject

class AuthenticateByUsernameUseCase @Inject constructor(
  private val userPurchasesRepository: IUserPurchasesRepository
) {
  fun invoke(username: String): Observable<Lce<AuthenticationViewState>> {
    return userPurchasesRepository.getAllUsers()
      .map<Lce<AuthenticationViewState>> {
        if (it.any { user -> user.username == username })
          Lce.Content(AuthenticationViewState(username = username, isAuthenticated = true))
        else
          Lce.Content(AuthenticationViewState(username = username, isAuthenticated = false))
      }
      .onErrorReturn {
        Lce.Error(AuthenticationViewState(username = username, isAuthenticated = false))
      }
      .toObservable()
      .startWith(Lce.Loading())
  }
}