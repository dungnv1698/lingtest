package com.example.domain.usecase.productdetail

import com.example.domain.entity.ProductDetail

data class ProductDetailViewState(
  val productId: Int,
  val productDetail: ProductDetail
)

sealed class ProductDetailViewEffect {
  object NotifyLoadFail: ProductDetailViewEffect()
  object ProductNotFound: ProductDetailViewEffect()
}

sealed class ProductDetailViewEvent {
  data class ScreenLoad(val productId: Int): ProductDetailViewEvent()
}