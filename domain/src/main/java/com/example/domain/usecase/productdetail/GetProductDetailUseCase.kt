package com.example.domain.usecase.productdetail

import com.example.domain.entity.Product
import com.example.domain.entity.ProductDetail
import com.example.domain.entity.Purchase
import com.example.domain.repository.IUserPurchasesRepository
import com.example.domain.util.Lce
import io.reactivex.Observable
import io.reactivex.functions.BiFunction
import javax.inject.Inject

class GetProductDetailUseCase @Inject constructor(
  private val userPurchasesRepository: IUserPurchasesRepository
) {
  fun invoke(productId: Int): Observable<Lce<ProductDetailViewState>> {
    return userPurchasesRepository.getProductInformation(productId).zipWith<List<Purchase>,Lce<ProductDetailViewState>>(
        userPurchasesRepository.getPurchasesByProduct(productId), BiFunction { product, purchasesByProduct ->
      Lce.Content(ProductDetailViewState(productId, mergeToProductDetail(product, purchasesByProduct)))
    }).onErrorReturn {
      Lce.Error(
          ProductDetailViewState(productId = productId, productDetail = ProductDetail())
      )
    }
        .toObservable()
        .startWith(Lce.Loading())
  }

  private fun mergeToProductDetail(product: Product, purchasesByProduct: List<Purchase>): ProductDetail {
    return ProductDetail(
        id = product.id,
        face = product.face,
        price = product.price,
        size = product.size,
        recent = purchasesByProduct.map { pch -> pch.username }
    )
  }
}