package com.example.domain.usecase.popularpurchase

import com.example.domain.entity.ProductDetail

data class PopularPurchaseViewState(
  val username: String,
  val content: List<ProductDetail>
)

sealed class PopularPurchaseViewEffect {
  object NotifyLoadFail: PopularPurchaseViewEffect()
  data class NavigateToProductDetail(val productId: Int): PopularPurchaseViewEffect()
}

sealed class PopularPurchaseViewEvent {
  data class ScreenLoad(val username: String): PopularPurchaseViewEvent()
  data class ClickProduct(val productId: Int): PopularPurchaseViewEvent()
}