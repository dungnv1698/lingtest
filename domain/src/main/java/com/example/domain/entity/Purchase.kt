package com.example.domain.entity

import java.util.Date

data class Purchase (
  val id: Int,
  val username: String,
  val productId: Int,
  val date: Date
)