package com.example.domain.entity

data class User (
  val username: String,
  val email: String
)