package com.example.domain.entity

data class ProductDetail (
  val id: Int = 0,
  val face: String = "",
  val price: Double = -1.0,
  val size: Int = 0,
  val recent: List<String> = emptyList()
)
