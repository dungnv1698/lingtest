package com.example.domain.entity

data class Product (
  val id: Int,
  val face: String = "",
  val price: Double = 0.0,
  val size: Int = -1
)