package com.example.domain.repository

import com.example.domain.entity.Product
import com.example.domain.entity.Purchase
import com.example.domain.entity.User
import io.reactivex.Single

interface IUserPurchasesRepository {
  fun getAllUsers(): Single<List<User>>
  fun getPurchasesByUsername(username: String, limit: Int): Single<List<Purchase>>
  fun getPurchasesByProduct(productId: Int): Single<List<Purchase>>
  fun getProductInformation(productId: Int): Single<Product>
}