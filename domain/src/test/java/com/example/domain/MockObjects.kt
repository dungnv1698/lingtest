package com.example.domain

import com.example.domain.entity.Product
import com.example.domain.entity.ProductDetail
import com.example.domain.entity.Purchase
import com.example.domain.entity.User
import java.util.Date

object MockObjects {
  val limitRecentPurchase = 5

  val users = listOf(
      User(username = "a", email = "a@gmail"),
      User(username = "b", email = "b@gmail"),
      User(username = "c", email = "c@gmail"),
      User(username = "d", email = "d@gmail"),
      User(username = "e", email = "e@gmail")
  )

  val product1 = Product(id = 1, face = "face1", price = 1.0, size = 1)
  val product2 = Product(id = 2, face = "face2", price = 2.0, size = 2)
  val product3 = Product(id = 3, face = "face3", price = 3.0, size = 3)
  val product4 = Product(id = 4, face = "face4", price = 4.0, size = 4)
  val product5 = Product(id = 5, face = "face5", price = 5.0, size = 5)

  val purchases = listOf(
    Purchase(1, "a", 5, Date()),

    Purchase(2, "b", 4, Date()),
    Purchase(3, "b", 5, Date()),

    Purchase(4, "c", 3, Date()),
    Purchase(5, "c", 4, Date()),
    Purchase(6, "c", 5, Date()),

    Purchase(7, "d", 2, Date()),
    Purchase(8, "d", 3, Date()),
    Purchase(9, "d", 4, Date()),
    Purchase(10, "d", 5, Date()),

    Purchase(11, "e", 1, Date()),
    Purchase(12, "e", 2, Date()),
    Purchase(13, "e", 3, Date()),
    Purchase(14, "e", 4, Date()),
    Purchase(15, "e", 5, Date()),
    Purchase(16, "e", 5, Date())
  )

  val peoplePurchase1 = listOf("e")
  val peoplePurchase2 = listOf("d", "e")
  val peoplePurchase3 = listOf("c", "d", "e")
  val peoplePurchase4 = listOf("b", "c", "d", "e")
  val peoplePurchase5 = listOf("a", "b", "c", "d", "e", "e")

  val productDetail1 = ProductDetail(id = product1.id, face = product1.face, price = product1.price, size = product1.size, recent = peoplePurchase1)
  val productDetail2 = ProductDetail(id = product2.id, face = product2.face, price = product2.price, size = product2.size, recent = peoplePurchase2)
  val productDetail3 = ProductDetail(id = product3.id, face = product3.face, price = product3.price, size = product3.size, recent = peoplePurchase3)
  val productDetail4 = ProductDetail(id = product4.id, face = product4.face, price = product4.price, size = product4.size, recent = peoplePurchase4)
  val productDetail5 = ProductDetail(id = product5.id, face = product5.face, price = product5.price, size = product5.size, recent = peoplePurchase5)
  
  

  fun mergeInforAndPeoplePurchasedToProductDetail(product: Product, peoplePurchased: List<String>): ProductDetail {
    return ProductDetail(
        id = product.id,
        face = product.face,
        price = product.price,
        size = product.size,
        recent = peoplePurchased
    )
  }
}