package com.example.domain.usecase.productdetail

import com.example.domain.MockObjects
import com.example.domain.MockObjects.product1
import com.example.domain.MockObjects.productDetail1
import com.example.domain.entity.ProductDetail
import com.example.domain.repository.IUserPurchasesRepository
import com.example.domain.util.Lce
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import java.lang.Exception

class GetProductDetailUseCaseTest {
  @Mock
  private lateinit var userPurchaseRepository: IUserPurchasesRepository

  private lateinit var getProductDetailUseCase: GetProductDetailUseCase

  @Before
  fun setUp() {
    userPurchaseRepository = Mockito.mock(IUserPurchasesRepository::class.java)
    getProductDetailUseCase = GetProductDetailUseCase(userPurchaseRepository)
  }

  @Test
  fun getProductDetailShouldReturnServerErrorWhenGetProductFail() {
    Mockito.`when`(userPurchaseRepository.getProductInformation(product1.id)).thenReturn(Single.error(Exception()))
    Mockito.`when`(userPurchaseRepository.getPurchasesByProduct(1)).thenReturn(Single.just(
        MockObjects.purchases.filter { pc -> pc.productId == 1 }))
    getProductDetailUseCase.invoke(product1.id)
        .test()
        .assertValueCount(2)
        .assertValueAt(0) { it is Lce.Loading }
        .assertValueAt(1) {
          it is Lce.Error && it.packet == ProductDetailViewState(1, ProductDetail())
        }
  }

  @Test
  fun getProductDetailShouldReturnServerErrorWhenGetPeoplePurchasedFail() {
    Mockito.`when`(userPurchaseRepository.getProductInformation(product1.id)).thenReturn(Single.just(product1))
    Mockito.`when`(userPurchaseRepository.getPurchasesByProduct(product1.id)).thenReturn(Single.error(Exception()))
    getProductDetailUseCase.invoke(product1.id)
        .test()
        .assertValueCount(2)
        .assertValueAt(0) { it is Lce.Loading }
        .assertValueAt(1) {
          it is Lce.Error && it.packet == ProductDetailViewState(1, ProductDetail())
        }
  }

  @Test
  fun getProductDetailShouldReturnServerErrorWhenGetProductAndPeoplePurchaseFail() {
    Mockito.`when`(userPurchaseRepository.getProductInformation(product1.id)).thenReturn(Single.error(Exception()))
    Mockito.`when`(userPurchaseRepository.getPurchasesByProduct(product1.id)).thenReturn(Single.error(Exception()))
    getProductDetailUseCase.invoke(product1.id)
        .test()
        .assertValueCount(2)
        .assertValueAt(0) { it is Lce.Loading }
        .assertValueAt(1) {
          it is Lce.Error && it.packet == ProductDetailViewState(1, ProductDetail())
        }
  }

  @Test
  fun getProductDetailShouldReturnResultWhenSuccess() {
    Mockito.`when`(userPurchaseRepository.getProductInformation(product1.id)).thenReturn(Single.just(product1))
    Mockito.`when`(userPurchaseRepository.getPurchasesByProduct(1)).thenReturn(Single.just(
        MockObjects.purchases.filter { pc -> pc.productId == 1 }))
    getProductDetailUseCase.invoke(product1.id)
        .test()
        .assertValueCount(2)
        .assertValueAt(0) { it is Lce.Loading }
        .assertValueAt(1) {
          it is Lce.Content && it.packet == ProductDetailViewState(1, productDetail1)
        }
  }
}