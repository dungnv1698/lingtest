package com.example.domain.usecase.authentication

import com.example.domain.MockObjects
import com.example.domain.repository.IUserPurchasesRepository
import com.example.domain.util.Lce
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import java.lang.Exception

class AuthenticateByUsernameUseCaseTest {
  @Mock
  private lateinit var userPurchaseRepository: IUserPurchasesRepository

  private lateinit var authenticateByUsernameUseCase: AuthenticateByUsernameUseCase

  @Before
  fun setUp() {
    userPurchaseRepository = Mockito.mock(IUserPurchasesRepository::class.java)
    authenticateByUsernameUseCase = AuthenticateByUsernameUseCase(userPurchaseRepository)
  }

  class ServerErrorException(): Exception()
  @Test
  fun authenticateShouldReturnErrorAuthenticateWhenError() {
    Mockito.`when`(userPurchaseRepository.getAllUsers()).thenReturn(Single.error(ServerErrorException()))
    authenticateByUsernameUseCase.invoke("").test()
      .assertComplete()
      .assertValueCount(2)
      .assertValueAt(0) {it is Lce.Loading}
      .assertValueAt(1) {it is Lce.Error && it.packet == AuthenticationViewState("", false) }
  }

  @Test
  fun authenticateShouldReturnUserNotExistWhenNotExist() {
    Mockito.`when`(userPurchaseRepository.getAllUsers()).thenReturn(Single.just(MockObjects.users))
    authenticateByUsernameUseCase.invoke("f").test()
      .assertComplete()
      .assertValueCount(2)
      .assertValueAt(0) {it is Lce.Loading}
      .assertValueAt(1) {it is Lce.Content && it.packet == AuthenticationViewState("f", false) }
  }

  @Test
  fun authenticateShouldReturnUserExistWhenExist() {
    Mockito.`when`(userPurchaseRepository.getAllUsers()).thenReturn(Single.just(MockObjects.users))
    authenticateByUsernameUseCase.invoke("a").test()
      .assertComplete()
      .assertValueCount(2)
      .assertValueAt(0) {it is Lce.Loading}
      .assertValueAt(1) {it is Lce.Content && it.packet == AuthenticationViewState("a", true) }
  }

  @Test
  fun authenticateShouldReturnUserNotExistWhenEmptyUsersList() {
    Mockito.`when`(userPurchaseRepository.getAllUsers()).thenReturn(Single.just(emptyList()))
    authenticateByUsernameUseCase.invoke("a").test()
      .assertComplete()
      .assertValueCount(2)
      .assertValueAt(0) {it is Lce.Loading}
      .assertValueAt(1) {it is Lce.Content && it.packet == AuthenticationViewState("a", false) }
  }
}