package com.example.domain.usecase.popularpurchase

import com.example.domain.MockObjects.limitRecentPurchase
import com.example.domain.MockObjects.mergeInforAndPeoplePurchasedToProductDetail
import com.example.domain.MockObjects.peoplePurchase1
import com.example.domain.MockObjects.peoplePurchase2
import com.example.domain.MockObjects.peoplePurchase3
import com.example.domain.MockObjects.peoplePurchase4
import com.example.domain.MockObjects.peoplePurchase5
import com.example.domain.MockObjects.product1
import com.example.domain.MockObjects.product2
import com.example.domain.MockObjects.product3
import com.example.domain.MockObjects.product4
import com.example.domain.MockObjects.product5
import com.example.domain.MockObjects.productDetail1
import com.example.domain.MockObjects.productDetail2
import com.example.domain.MockObjects.productDetail3
import com.example.domain.MockObjects.productDetail4
import com.example.domain.MockObjects.productDetail5
import com.example.domain.MockObjects.purchases
import com.example.domain.entity.Product
import com.example.domain.entity.ProductDetail
import com.example.domain.repository.IUserPurchasesRepository
import com.example.domain.util.Lce
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.mockito.Mock
import org.mockito.Mockito
import java.lang.Exception

class GetPopularPurchasesUseCaseTest {
  @Mock
  private lateinit var userPurchaseRepository: IUserPurchasesRepository

  private lateinit var getPopularPurchasesUseCase: GetPopularPurchasesUseCase

  @Before
  fun setUp() {
    userPurchaseRepository = Mockito.mock(IUserPurchasesRepository::class.java)
    getPopularPurchasesUseCase = GetPopularPurchasesUseCase(userPurchaseRepository)
  }

  class GetPopularException(): Exception()
  @Test
  fun getPopularPurchasesShouldReturnErrorWhenGetPurchasesFail() {
    Mockito.`when`(userPurchaseRepository.getPurchasesByUsername("a", limitRecentPurchase))
      .thenReturn(Single.error(GetPopularException()))
    getPopularPurchasesUseCase.invoke("a", limitRecentPurchase)
      .test()
      .assertValueCount(2)
      .assertValueAt(0) {it is Lce.Loading}
      .assertValueAt(1) {it is Lce.Error && it.packet == PopularPurchaseViewState("a", emptyList()) }
  }

  @Test
  fun getPopularPurchasesShouldReturnSortedProducts() {
    Mockito.`when`(userPurchaseRepository.getPurchasesByUsername("e", limitRecentPurchase))
      .thenReturn(Single.just(purchases.filter { pc -> pc.username == "e" }))
    Mockito.`when`(userPurchaseRepository.getProductInformation(1)).thenReturn(Single.just(product1))
    Mockito.`when`(userPurchaseRepository.getProductInformation(2)).thenReturn(Single.just(product2))
    Mockito.`when`(userPurchaseRepository.getProductInformation(3)).thenReturn(Single.just(product3))
    Mockito.`when`(userPurchaseRepository.getProductInformation(4)).thenReturn(Single.just(product4))
    Mockito.`when`(userPurchaseRepository.getProductInformation(5)).thenReturn(Single.just(product5))

    Mockito.`when`(userPurchaseRepository.getPurchasesByProduct(1)).thenReturn(Single.just(purchases.filter { pc -> pc.productId == 1 }))
    Mockito.`when`(userPurchaseRepository.getPurchasesByProduct(2)).thenReturn(Single.just(purchases.filter { pc -> pc.productId == 2 }))
    Mockito.`when`(userPurchaseRepository.getPurchasesByProduct(3)).thenReturn(Single.just(purchases.filter { pc -> pc.productId == 3 }))
    Mockito.`when`(userPurchaseRepository.getPurchasesByProduct(4)).thenReturn(Single.just(purchases.filter { pc -> pc.productId == 4 }))
    Mockito.`when`(userPurchaseRepository.getPurchasesByProduct(5)).thenReturn(Single.just(purchases.filter { pc -> pc.productId == 5 }))

    getPopularPurchasesUseCase.invoke("e", limitRecentPurchase)
      .test()
      .assertValueCount(2)
      .assertValueAt(0) { it is Lce.Loading }
      .assertValueAt(1) {
        it is Lce.Content && it.packet == PopularPurchaseViewState("e",
          listOf(
            productDetail5,
            productDetail4,
            productDetail3,
            productDetail2,
            productDetail1
          )
        )
      }
  }

  @Test
  fun getPopularPurchasesShouldReturnSortedProductsWhenOneOrMoreGetPeoplePurchaseFail() {
    Mockito.`when`(userPurchaseRepository.getPurchasesByUsername("e", limitRecentPurchase))
      .thenReturn(Single.just(purchases.filter { pc -> pc.username == "e" }))
    Mockito.`when`(userPurchaseRepository.getProductInformation(1)).thenReturn(Single.just(product1))
    Mockito.`when`(userPurchaseRepository.getProductInformation(2)).thenReturn(Single.just(product2))
    Mockito.`when`(userPurchaseRepository.getProductInformation(3)).thenReturn(Single.just(product3))
    Mockito.`when`(userPurchaseRepository.getProductInformation(4)).thenReturn(Single.just(product4))
    Mockito.`when`(userPurchaseRepository.getProductInformation(5)).thenReturn(Single.just(product5))

    Mockito.`when`(userPurchaseRepository.getPurchasesByProduct(1)).thenReturn(Single.error(Exception()))
    Mockito.`when`(userPurchaseRepository.getPurchasesByProduct(2)).thenReturn(Single.just(purchases.filter { pc -> pc.productId == 2 }))
    Mockito.`when`(userPurchaseRepository.getPurchasesByProduct(3)).thenReturn(Single.error(Exception()))
    Mockito.`when`(userPurchaseRepository.getPurchasesByProduct(4)).thenReturn(Single.just(purchases.filter { pc -> pc.productId == 4 }))
    Mockito.`when`(userPurchaseRepository.getPurchasesByProduct(5)).thenReturn(Single.error(Exception()))

    getPopularPurchasesUseCase.invoke("e", limitRecentPurchase)
      .test()
      .assertValueCount(2)
      .assertValueAt(0) { it is Lce.Loading }
      .assertValueAt(1) {
        it is Lce.Content && it.packet == PopularPurchaseViewState("e",
          listOf(
            productDetail4,
            productDetail2,
            productDetail1.copy(recent = emptyList()),
            productDetail3.copy(recent = emptyList()),
            productDetail5.copy(recent = emptyList())
          )
        )
      }
  }

  @Test
  fun getPopularPurchasesShouldReturnSortedProductsWhenOneOrMoreGetProductInformationFail() {
    Mockito.`when`(userPurchaseRepository.getPurchasesByUsername("e", limitRecentPurchase))
      .thenReturn(Single.just(purchases.filter { pc -> pc.username == "e" }))
    Mockito.`when`(userPurchaseRepository.getProductInformation(1)).thenReturn(Single.error(Exception()))
    Mockito.`when`(userPurchaseRepository.getProductInformation(2)).thenReturn(Single.just(product2))
    Mockito.`when`(userPurchaseRepository.getProductInformation(3)).thenReturn(Single.just(product3))
    Mockito.`when`(userPurchaseRepository.getProductInformation(4)).thenReturn(Single.just(product4))
    Mockito.`when`(userPurchaseRepository.getProductInformation(5)).thenReturn(Single.just(product5))

    Mockito.`when`(userPurchaseRepository.getPurchasesByProduct(1)).thenReturn(Single.just(purchases.filter { pc -> pc.productId == 1 }))
    Mockito.`when`(userPurchaseRepository.getPurchasesByProduct(2)).thenReturn(Single.just(purchases.filter { pc -> pc.productId == 2 }))
    Mockito.`when`(userPurchaseRepository.getPurchasesByProduct(3)).thenReturn(Single.just(purchases.filter { pc -> pc.productId == 3 }))
    Mockito.`when`(userPurchaseRepository.getPurchasesByProduct(4)).thenReturn(Single.just(purchases.filter { pc -> pc.productId == 4 }))
    Mockito.`when`(userPurchaseRepository.getPurchasesByProduct(5)).thenReturn(Single.just(purchases.filter { pc -> pc.productId == 5 }))

    getPopularPurchasesUseCase.invoke("e", limitRecentPurchase)
      .test()
      .assertValueCount(2)
      .assertValueAt(0) { it is Lce.Loading }
      .assertValueAt(1) {
        it is Lce.Content && it.packet == PopularPurchaseViewState("e",
          listOf(
            productDetail5,
            productDetail4,
            productDetail3,
            productDetail2,
            Product(id = 1).run {
              ProductDetail(id = id, face = face, size = size, price = price, recent = peoplePurchase1)
            }
          )
        )
      }
  }

  @Test
  fun getPopularPurchasesShouldReturn5ProductsWhen2PurchasesSameProduct() {
    Mockito.`when`(userPurchaseRepository.getPurchasesByUsername("e", limitRecentPurchase))
        .thenReturn(Single.just(purchases.filter { pc -> pc.username == "e" }))
    Mockito.`when`(userPurchaseRepository.getProductInformation(1)).thenReturn(Single.just(product1))
    Mockito.`when`(userPurchaseRepository.getProductInformation(2)).thenReturn(Single.just(product2))
    Mockito.`when`(userPurchaseRepository.getProductInformation(3)).thenReturn(Single.just(product3))
    Mockito.`when`(userPurchaseRepository.getProductInformation(4)).thenReturn(Single.just(product4))
    Mockito.`when`(userPurchaseRepository.getProductInformation(5)).thenReturn(Single.just(product5))

    Mockito.`when`(userPurchaseRepository.getPurchasesByProduct(1)).thenReturn(Single.just(purchases.filter { pc -> pc.productId == 1 }))
    Mockito.`when`(userPurchaseRepository.getPurchasesByProduct(2)).thenReturn(Single.just(purchases.filter { pc -> pc.productId == 2 }))
    Mockito.`when`(userPurchaseRepository.getPurchasesByProduct(3)).thenReturn(Single.just(purchases.filter { pc -> pc.productId == 3 }))
    Mockito.`when`(userPurchaseRepository.getPurchasesByProduct(4)).thenReturn(Single.just(purchases.filter { pc -> pc.productId == 4 }))
    Mockito.`when`(userPurchaseRepository.getPurchasesByProduct(5)).thenReturn(Single.just(purchases.filter { pc -> pc.productId == 5 }))

    getPopularPurchasesUseCase.invoke("e", limitRecentPurchase)
        .test()
        .assertValueCount(2)
        .assertValueAt(0) { it is Lce.Loading }
        .assertValueAt(1) {
          // Maximum is 5 products
          if (it is Lce.Content)
            assert(it.packet.content.size == 5)
          it is Lce.Content && it.packet == PopularPurchaseViewState("e",
              listOf(
                  productDetail5,
                  productDetail4,
                  productDetail3,
                  productDetail2,
                  productDetail1
              )
          )
        }
  }
}