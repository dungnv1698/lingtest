package com.example.presentation.util

import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.*
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

// Create view model extension
fun <T : ViewModel> AppCompatActivity.createViewModel(
    factory: ViewModelProvider.Factory, classType: Class<T>
): T = ViewModelProvider(this, factory)[classType]

// Rxjava extension
fun <T> Observable<T>.applyIOScheduler(): Observable<T> {
    return this
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
}

fun <T> Observable<T>.applyComputation(): Observable<T> {
    return this
        .subscribeOn(Schedulers.computation())
        .observeOn(AndroidSchedulers.mainThread())
}

fun <T> Observable<T>.toMainThread(): Observable<T> {
    return this
        .observeOn(AndroidSchedulers.mainThread())
}

fun <T> Single<T>.applyIOScheduler(): Single<T> {
    return this
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
}

fun <T> Single<T>.applyComputation(): Single<T> {
    return this
        .subscribeOn(Schedulers.computation())
        .observeOn(AndroidSchedulers.mainThread())
}

fun <T> Single<T>.toMainThread(): Single<T> {
    return this
        .observeOn(AndroidSchedulers.mainThread())
}

fun Disposable.addToCompositeDisposable(compositeDisposable: CompositeDisposable) {
    if(!compositeDisposable.isDisposed) {
        compositeDisposable.add(this)
    }
}