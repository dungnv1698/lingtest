package com.example.presentation.screen.popularpurchase

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.domain.entity.ProductDetail
import com.example.presentation.databinding.PopularpurchaseProductItemBinding

class PopularPurchaseAdapter(
  private val productListBehavior: IProductListBehavior
): RecyclerView.Adapter<ProductViewHolder>() {
  private val products = arrayListOf<ProductDetail>()

  override fun onCreateViewHolder(
    parent: ViewGroup,
    viewType: Int
  ): ProductViewHolder {
    return ProductViewHolder(
        PopularpurchaseProductItemBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        ),
        productListBehavior
    )
  }

  override fun getItemCount() = products.size

  override fun onBindViewHolder(
    holder: ProductViewHolder,
    position: Int
  ) {
    holder.bind(products[position])
  }

  fun submitList(productList: List<ProductDetail>) {
    products.clear()
    products.addAll(productList)
    notifyDataSetChanged()
  }
}

class ProductViewHolder(
  private val binding: PopularpurchaseProductItemBinding,
  private val productListBehavior: IProductListBehavior
) : RecyclerView.ViewHolder(binding.root) {
  fun bind(product: ProductDetail) {
    binding.product = product
    binding.listener = productListBehavior
  }
}




