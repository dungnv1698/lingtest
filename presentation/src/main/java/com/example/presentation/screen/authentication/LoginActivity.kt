package com.example.presentation.screen.authentication

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.example.domain.usecase.authentication.AuthenticateViewEffect
import com.example.domain.usecase.authentication.AuthenticateViewEffect.NavigateToPopularPurchase
import com.example.domain.usecase.authentication.AuthenticateViewEffect.NotifyAuthenticateFail
import com.example.domain.usecase.authentication.AuthenticateViewEffect.NotifyErrorAuthenticate
import com.example.domain.usecase.authentication.AuthenticateViewEvent
import com.example.presentation.R
import com.example.presentation.databinding.ActivityLoginBinding
import com.example.presentation.screen.popularpurchase.PopularPurchaseActivity
import com.example.presentation.util.createViewModel
import com.example.presentation.util.toMainThread
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

class LoginActivity : DaggerAppCompatActivity() {
  companion object {
    const val UsernameExtraKey = "USERNAME_EXTRA"
  }

  @Inject
  lateinit var factory: ViewModelProvider.Factory
  private lateinit var binding: ActivityLoginBinding

  private val viewModel: LoginViewModel by lazy {
    createViewModel(factory, LoginViewModel::class.java)
  }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    binding = DataBindingUtil.setContentView(this, R.layout.activity_login)
    setUpView()
  }

  override fun onResume() {
    super.onResume()
    viewModel.viewEffect.toMainThread()
      .subscribe(::onViewEffect)
  }

  private fun onViewEffect(effect: AuthenticateViewEffect) {
    when(effect) {
      is NotifyAuthenticateFail -> {
        Toast.makeText(this, "User with username of '${effect.username}' was not found ", Toast.LENGTH_SHORT).show()
      }
      is NavigateToPopularPurchase -> {
        openPopularPurchaseActivity(effect.username)
      }
      NotifyErrorAuthenticate -> {
        Toast.makeText(this, "There are error when login", Toast.LENGTH_SHORT).show()
      }
    }
  }

  private fun openPopularPurchaseActivity(username: String) {
    val intent = Intent(this, PopularPurchaseActivity::class.java).apply {
      putExtra(UsernameExtraKey, username)
    }
    startActivity(intent)
    finish()
  }

  private fun setUpView() {
    binding.btnLogin.setOnClickListener {
      viewModel.processInput(AuthenticateViewEvent.Login(binding.edtUsername.text.toString()))
    }
  }
}