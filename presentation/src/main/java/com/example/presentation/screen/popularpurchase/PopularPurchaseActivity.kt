package com.example.presentation.screen.popularpurchase

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.domain.usecase.popularpurchase.PopularPurchaseViewEffect
import com.example.domain.usecase.popularpurchase.PopularPurchaseViewEffect.NavigateToProductDetail
import com.example.domain.usecase.popularpurchase.PopularPurchaseViewEffect.NotifyLoadFail
import com.example.domain.usecase.popularpurchase.PopularPurchaseViewEvent
import com.example.domain.usecase.popularpurchase.PopularPurchaseViewEvent.ScreenLoad
import com.example.domain.usecase.popularpurchase.PopularPurchaseViewState
import com.example.presentation.R
import com.example.presentation.databinding.ActivityPopularPurchaseBinding
import com.example.presentation.screen.authentication.LoginActivity.Companion.UsernameExtraKey
import com.example.presentation.screen.productdetail.ProductDetailActivity
import com.example.presentation.util.createViewModel
import com.example.presentation.util.toMainThread
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

class PopularPurchaseActivity : DaggerAppCompatActivity() {
  companion object {
    const val ProductIdExtraKey = "PRODUCTID_KEY"
  }

  @Inject
  lateinit var factory: ViewModelProvider.Factory
  private lateinit var binding: ActivityPopularPurchaseBinding

  private val viewModel: PopularPurchaseViewModel by lazy {
    createViewModel(factory, PopularPurchaseViewModel::class.java)
  }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    binding = DataBindingUtil.setContentView(this, R.layout.activity_popular_purchase)
    setUpView()

    viewModel.viewEffect.toMainThread()
        .subscribe(::onViewEffect)
    viewModel.viewState.observe(this, Observer(::onViewState))

    setUpData()
  }

  private fun onViewEffect(effect: PopularPurchaseViewEffect) {
    when(effect) {
      NotifyLoadFail -> Toast.makeText(this, "Load popular purchase fail", Toast.LENGTH_SHORT).show()
      is NavigateToProductDetail -> {
        openProductDetailScreen(effect.productId)
      }
    }
  }

  private fun onViewState(state: PopularPurchaseViewState) {
    (binding.rvProducts.adapter as PopularPurchaseAdapter).submitList(state.content)
  }

  private fun setUpData() {
    val username = intent.getStringExtra(UsernameExtraKey)
    viewModel.processInput(ScreenLoad(username ?: return))
  }

  private fun setUpView() {
    binding.rvProducts.layoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
    binding.rvProducts.adapter = PopularPurchaseAdapter(object : IProductListBehavior {
      override fun onProductSelected(productId: Int) {
        viewModel.processInput(PopularPurchaseViewEvent.ClickProduct(productId))
      }
    })
  }

  private fun openProductDetailScreen(productId: Int) {
    val intent = Intent(this, ProductDetailActivity::class.java).apply {
      putExtra(ProductIdExtraKey, productId)
    }

    startActivity(intent)
  }
}