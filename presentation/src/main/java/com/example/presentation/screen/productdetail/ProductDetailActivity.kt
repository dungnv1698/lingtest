package com.example.presentation.screen.productdetail

import android.os.Bundle
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.example.domain.usecase.productdetail.ProductDetailViewEffect.NotifyLoadFail
import com.example.domain.usecase.productdetail.ProductDetailViewEffect.ProductNotFound
import com.example.domain.usecase.productdetail.ProductDetailViewEvent
import com.example.domain.usecase.productdetail.ProductDetailViewState
import com.example.presentation.R
import com.example.presentation.databinding.ActivityProductDetailBinding
import com.example.presentation.screen.popularpurchase.PopularPurchaseActivity
import com.example.presentation.util.createViewModel
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject

class ProductDetailActivity : DaggerAppCompatActivity() {
  @Inject
  lateinit var factory: ViewModelProvider.Factory
  private lateinit var binding: ActivityProductDetailBinding

  private val viewModel: ProductDetailViewModel by lazy {
    createViewModel(factory, ProductDetailViewModel::class.java)
  }

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    binding = DataBindingUtil.setContentView(this, R.layout.activity_product_detail)

    viewModel.viewState.observe(this, Observer(::onViewState))
    viewModel.viewEffect.subscribe {
      when (it) {
        NotifyLoadFail -> Toast.makeText(this, "Can't load product detail!", Toast.LENGTH_SHORT).show()
        ProductNotFound -> Toast.makeText(this, "Product is not found!", Toast.LENGTH_SHORT).show()
      }
    }

    loadData()
  }

  private fun onViewState(state: ProductDetailViewState) {
    binding.product = state.productDetail
  }

  private fun loadData() {
    val productId = intent.getIntExtra(PopularPurchaseActivity.ProductIdExtraKey, -1)
    viewModel.processInput(ProductDetailViewEvent.ScreenLoad(productId))
  }
}