package com.example.presentation.screen.productdetail

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.domain.usecase.productdetail.GetProductDetailUseCase
import com.example.domain.usecase.productdetail.ProductDetailViewEffect
import com.example.domain.usecase.productdetail.ProductDetailViewEvent
import com.example.domain.usecase.productdetail.ProductDetailViewEvent.ScreenLoad
import com.example.domain.usecase.productdetail.ProductDetailViewState
import com.example.domain.util.Lce
import com.example.presentation.base.BaseViewModel
import com.example.presentation.util.addToCompositeDisposable
import com.example.presentation.util.applyIOScheduler
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

class ProductDetailViewModel @Inject constructor(
  private val getProductDetailUseCase: GetProductDetailUseCase
) : BaseViewModel() {
  private val _viewState = MutableLiveData<ProductDetailViewState>()
  val viewState: LiveData<ProductDetailViewState> get() = _viewState
  private val _viewEffect = PublishSubject.create<ProductDetailViewEffect>()
  val viewEffect: Observable<ProductDetailViewEffect> = _viewEffect

  fun processInput(event: ProductDetailViewEvent) {
    when (event) {
      is ScreenLoad -> screenLoad(event.productId)
    }
  }

  private fun screenLoad(productId: Int) {
    if (productId < 0) {
      _viewEffect.onNext(ProductDetailViewEffect.ProductNotFound)
    } else {
      getProductDetailUseCase.invoke(productId)
          .applyIOScheduler()
          .subscribe({
            when (it) {
              is Lce.Loading -> {
              }
              is Lce.Content -> {
                _viewState.postValue(it.packet)
              }
              is Lce.Error -> {
                _viewEffect.onNext(ProductDetailViewEffect.NotifyLoadFail)
              }
            }
          }, {
            _viewEffect.onNext(ProductDetailViewEffect.NotifyLoadFail)
          })
          .addToCompositeDisposable(compositeDisposable)
    }
  }
}