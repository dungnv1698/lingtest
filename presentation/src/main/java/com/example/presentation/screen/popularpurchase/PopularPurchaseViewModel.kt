package com.example.presentation.screen.popularpurchase

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.domain.usecase.popularpurchase.GetPopularPurchasesUseCase
import com.example.domain.usecase.popularpurchase.PopularPurchaseViewEffect
import com.example.domain.usecase.popularpurchase.PopularPurchaseViewEvent
import com.example.domain.usecase.popularpurchase.PopularPurchaseViewEvent.ClickProduct
import com.example.domain.usecase.popularpurchase.PopularPurchaseViewEvent.ScreenLoad
import com.example.domain.usecase.popularpurchase.PopularPurchaseViewState
import com.example.domain.util.Lce
import com.example.presentation.base.BaseViewModel
import com.example.presentation.util.addToCompositeDisposable
import com.example.presentation.util.applyIOScheduler
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

class PopularPurchaseViewModel @Inject constructor(
  private val getPopularPurchasesUseCase: GetPopularPurchasesUseCase
) : BaseViewModel() {
  companion object {
    const val LimitPurchase = 5
  }

  private val _viewState = MutableLiveData<PopularPurchaseViewState>()
  val viewState: LiveData<PopularPurchaseViewState> get() = _viewState
  private val _viewEffect = PublishSubject.create<PopularPurchaseViewEffect>()
  val viewEffect: Observable<PopularPurchaseViewEffect> = _viewEffect

  fun processInput(event: PopularPurchaseViewEvent) {
    when (event) {
      is ScreenLoad -> screenLoad(event.username)
      is ClickProduct -> clickProduct(event.productId)
    }
  }

  private fun clickProduct(productId: Int) {
    _viewEffect.onNext(PopularPurchaseViewEffect.NavigateToProductDetail(productId))
  }

  private fun screenLoad(username: String) {
    getPopularPurchasesUseCase.invoke(username, LimitPurchase)
        .applyIOScheduler()
        .subscribe({
          when (it) {
            is Lce.Loading -> {
            }
            is Lce.Content -> {
              _viewState.postValue(it.packet)
            }
            is Lce.Error -> {
              _viewEffect.onNext(PopularPurchaseViewEffect.NotifyLoadFail)
            }
          }
        }, {
          _viewEffect.onNext(PopularPurchaseViewEffect.NotifyLoadFail)
        })
        .addToCompositeDisposable(compositeDisposable)
  }
}