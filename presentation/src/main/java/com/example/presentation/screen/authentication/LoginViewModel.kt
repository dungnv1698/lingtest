package com.example.presentation.screen.authentication

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.example.domain.usecase.authentication.AuthenticateByUsernameUseCase
import com.example.domain.usecase.authentication.AuthenticateViewEffect
import com.example.domain.usecase.authentication.AuthenticateViewEvent
import com.example.domain.usecase.authentication.AuthenticateViewEvent.Login
import com.example.domain.usecase.authentication.AuthenticationViewState
import com.example.domain.util.Lce
import com.example.presentation.base.BaseViewModel
import com.example.presentation.util.addToCompositeDisposable
import com.example.presentation.util.applyIOScheduler
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import javax.inject.Inject

class LoginViewModel @Inject constructor(
  private val authenticateByUsernameUseCase: AuthenticateByUsernameUseCase
): BaseViewModel() {
  private val _viewState = MutableLiveData<AuthenticationViewState>()
  val viewState: LiveData<AuthenticationViewState> get() = _viewState
  private val _viewEffect = PublishSubject.create<AuthenticateViewEffect>()
  val viewEffect: Observable<AuthenticateViewEffect> = _viewEffect

  private fun login(username: String) {
    authenticateByUsernameUseCase.invoke(username)
      .applyIOScheduler()
      .subscribe ({
        when (it) {
          is Lce.Loading -> {
          }
          is Lce.Content -> {
            if (it.packet.isAuthenticated)
              _viewEffect.onNext(AuthenticateViewEffect.NavigateToPopularPurchase(it.packet.username))
            else
              _viewEffect.onNext(AuthenticateViewEffect.NotifyAuthenticateFail(it.packet.username))
          }
          is Lce.Error -> {
            _viewEffect.onNext(AuthenticateViewEffect.NotifyErrorAuthenticate)
          }
        }
      }, {
        _viewEffect.onNext(AuthenticateViewEffect.NotifyErrorAuthenticate)
      })
      .addToCompositeDisposable(compositeDisposable)
  }

  fun processInput(authenticateViewEvent: AuthenticateViewEvent) {
    when (authenticateViewEvent) {
      is Login -> login(authenticateViewEvent.username)
    }
  }
}