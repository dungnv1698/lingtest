package com.example.presentation.screen.popularpurchase

interface IProductListBehavior {
  fun onProductSelected(productId: Int)
}