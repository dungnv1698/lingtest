package com.example.data.repository

import com.example.data.api.IPurchaseAPI
import com.example.domain.entity.Product
import com.example.domain.entity.Purchase
import com.example.domain.entity.User
import com.example.domain.repository.IUserPurchasesRepository
import io.reactivex.Single
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class UserPurchasesRepository @Inject constructor(
  private val purchaseApi: IPurchaseAPI
): IUserPurchasesRepository {
  override fun getAllUsers(): Single<List<User>> {
    return purchaseApi.getAllUsers().map { it.users }
  }

  override fun getPurchasesByUsername(username: String, limit: Int): Single<List<Purchase>> {
    return purchaseApi.getPurchasesByUsername(username, limit).map { it.purchases }
  }

  override fun getPurchasesByProduct(productId: Int): Single<List<Purchase>> {
    return purchaseApi.getPurchasesByProduct(productId).map { it.purchases }
  }

  override fun getProductInformation(productId: Int): Single<Product> {
    return purchaseApi.getProductInformation(productId).map { it.product }
  }
}