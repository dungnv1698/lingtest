package com.example.data.api.response

import com.example.domain.entity.User
import com.google.gson.annotations.SerializedName

data class AllUsersResponse(
  @SerializedName("users") val users: List<User>
)