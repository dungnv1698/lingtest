package com.example.data.api.response

import com.example.domain.entity.Purchase
import com.google.gson.annotations.SerializedName

data class GetRecentPurchasesResponse(
  @SerializedName("purchases") val purchases: List<Purchase>
)