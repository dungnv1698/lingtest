package com.example.data.api.response

import com.example.domain.entity.Product
import com.google.gson.annotations.SerializedName

data class ProductionInformationResponse(
  @SerializedName("product") val product: Product
)