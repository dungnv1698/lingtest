package com.example.data.api

import com.example.data.api.response.AllUsersResponse
import com.example.data.api.response.GetRecentPurchasesResponse
import com.example.data.api.response.ProductionInformationResponse
import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface IPurchaseAPI {
  @GET("users")
  fun getAllUsers(): Single<AllUsersResponse>

  @GET("purchases/by_user/{username}")
  fun getPurchasesByUsername(
    @Path("username") username: String,
    @Query("limit") limit: Int
  ): Single<GetRecentPurchasesResponse>

  @GET("purchases/by_product/{productId}")
  fun getPurchasesByProduct(@Path("productId") productId: Int): Single<GetRecentPurchasesResponse>

  @GET("products/{productId}")
  fun getProductInformation(@Path("productId") productId: Int): Single<ProductionInformationResponse>
}