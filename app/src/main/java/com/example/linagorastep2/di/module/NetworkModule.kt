package com.example.linagorastep2.di.module

import com.example.data.api.IPurchaseAPI
import com.example.linagorastep2.util.Constant
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton

@Module
class NetworkModule {
  @Provides
  @Singleton
  fun provideRetrofit(client: OkHttpClient): Retrofit {
    return Retrofit.Builder()
      .baseUrl(Constant.API_BASE_URL)
      .client(client)
      .addConverterFactory(GsonConverterFactory.create())
      .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
      .build()
  }

  @Provides
  @Singleton
  fun providePurchaseApi(retrofit: Retrofit): IPurchaseAPI {
    return retrofit.create(IPurchaseAPI::class.java)
  }

  @Provides
  @Singleton
  fun provideOkHttpClient(): OkHttpClient {
    val builder = OkHttpClient.Builder()

    // Add logging
    configLogging(builder)

    return builder.build()
  }

  private fun configLogging(clientBuilder: OkHttpClient.Builder) {
    val logging = HttpLoggingInterceptor()
    logging.level = HttpLoggingInterceptor.Level.BODY
    clientBuilder.addInterceptor(logging)
  }
}