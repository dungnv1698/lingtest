package com.example.linagorastep2.di.component

import com.example.linagorastep2.LingTestApplication
import com.example.linagorastep2.di.module.ActivityBindingModule
import com.example.linagorastep2.di.module.NetworkModule
import com.example.linagorastep2.di.module.RepositoryModule
import com.example.linagorastep2.di.module.ViewModelModule
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
  modules = [
    AndroidSupportInjectionModule::class,
    NetworkModule::class,
    RepositoryModule::class,
    ViewModelModule::class,
    ActivityBindingModule::class
  ]
)
interface AppComponent : AndroidInjector<LingTestApplication> {

}