package com.example.linagorastep2.di.module

import com.example.data.repository.UserPurchasesRepository
import com.example.domain.repository.IUserPurchasesRepository
import dagger.Binds
import dagger.Module

@Module
abstract class RepositoryModule {
  @Binds
  abstract fun provideUserPurchaseRepository(userPurchasesRepository: UserPurchasesRepository): IUserPurchasesRepository
}