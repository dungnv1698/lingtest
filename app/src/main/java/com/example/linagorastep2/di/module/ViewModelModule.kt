package com.example.linagorastep2.di.module

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.linagorastep2.di.qualifier.ViewModelKey
import com.example.presentation.screen.authentication.LoginViewModel
import com.example.presentation.screen.popularpurchase.PopularPurchaseViewModel
import com.example.presentation.screen.productdetail.ProductDetailViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelModule {
  @Binds
  @IntoMap
  @ViewModelKey(LoginViewModel::class)
  abstract fun bindLoginViewModel(viewModel: LoginViewModel): ViewModel

  @Binds
  @IntoMap
  @ViewModelKey(PopularPurchaseViewModel::class)
  abstract fun bindPopularPurchaseViewModel(viewModel: PopularPurchaseViewModel): ViewModel

  @Binds
  @IntoMap
  @ViewModelKey(ProductDetailViewModel::class)
  abstract fun bindProductDetailViewModel(viewModel: ProductDetailViewModel): ViewModel

  @Binds
  internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory
}