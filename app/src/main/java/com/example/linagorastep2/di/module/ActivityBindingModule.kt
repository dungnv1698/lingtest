package com.example.linagorastep2.di.module

import com.example.presentation.screen.authentication.LoginActivity
import com.example.presentation.screen.popularpurchase.PopularPurchaseActivity
import com.example.presentation.screen.productdetail.ProductDetailActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBindingModule {
  @ContributesAndroidInjector
  abstract fun getLoginActivity(): LoginActivity

  @ContributesAndroidInjector
  abstract fun getPopularPurchaseActivity(): PopularPurchaseActivity

  @ContributesAndroidInjector
  abstract fun getProductDetailActivity(): ProductDetailActivity
}